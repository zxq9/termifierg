%%% @doc
%%% Termifier GUI Controller
%%%
%%% This process is a in charge of maintaining the program's state.
%%% @end

-module(tg_con).
-vsn("0.1.2").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-behavior(gen_server).
-export([read_file/1, save_file/2]).
-export([start_link/0, stop/0]).
-export([init/1, terminate/2, code_change/3,
         handle_call/3, handle_cast/2, handle_info/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {window = none :: none | wx:wx_object()}).

-type state() :: #s{}.



%% Interface


read_file(Path) ->
    gen_server:call(?MODULE, {read_file, Path}).


save_file(Path, Data) ->
    gen_server:call(?MODULE, {save_file, Path, Data}).


-spec stop() -> ok.

stop() ->
    gen_server:cast(?MODULE, stop).



%%% Startup Functions


-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason},
         Reason :: {already_started, pid()}
                 | {shutdown, term()}
                 | term().
%% @private
%% Called by tg_sup.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec init(none) -> no_return().

init(none) ->
    ok = log(info, "Starting"),
    Window = tg_gui:start_link("Termifier GUI"),
    ok = log(info, "Window: ~p", [Window]),
    State = #s{window = Window},
    {ok, State}.



%%% gen_server Message Handling Callbacks


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().
%% @private
%% The gen_server:handle_call/3 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_call-3

handle_call({read_file, Path}, _, State) ->
    Result = do_read_file(Path),
    {reply, Result, State};
handle_call({save_file, Path, Data}, _, State) ->
    Result = do_save_file(Path, Data),
    {reply, Result, State};
handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_cast/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_cast-2

handle_cast(stop, State) ->
    ok = log(info, "Received a 'stop' message."),
%   {noreply, State};
    ok = zx:silent_stop(),
    {noreply, State};
handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_info/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2

handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.



%% @private
%% gen_server callback to handle state transformations necessary for hot
%% code updates. This template performs no transformation.

code_change(_, State, _) ->
    {ok, State}.


terminate(Reason, State) ->
    ok = log(info, "Reason: ~tp, State: ~tp", [Reason, State]),
    zx:stop().


do_read_file(Path) ->
    file:read_file(Path).


do_save_file(Path, Data) ->
    file:write_file(Path, [Data, "\n"]).

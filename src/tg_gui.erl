%%% @doc
%%% Termifier GUI GUI
%%%
%%% This process is responsible for creating the main GUI frame displayed to the user.
%%%
%%% Reference: http://erlang.org/doc/man/wx_object.html
%%% @end

-module(tg_gui).
-vsn("0.1.2").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-behavior(wx_object).
-include_lib("wx/include/wx.hrl").
-export([json/1]).
-export([start_link/1]).
-export([init/1, terminate/2, code_change/3,
         handle_call/3, handle_cast/2, handle_info/2, handle_event/2]).
-include("$zx_include/zx_logger.hrl").


-record(s,
        {frame  = none :: none | wx:wx_object(),
         json   = none :: none | wx:wx_object(),
         eterms = none :: none | wx:wx_object()}).


-type state() :: term().



%%% Labels

-define(sysREAD, 10).
-define(sysCONV, 11).
-define(sysSAVE, 12).



%%% Interface functions

json(String) ->
    wx_object:cast(?MODULE, {json, String}).



%%% Startup Functions

start_link(Title) ->
    wx_object:start_link({local, ?MODULE}, ?MODULE, Title, []).


init(Title) ->
    ok = log(info, "GUI starting..."),
    Wx = wx:new(),
    Frame = wxFrame:new(Wx, ?wxID_ANY, Title),
    State = build_interface(Frame),
    {Frame, State}.


build_interface(Frame) ->
    MainSz = wxBoxSizer:new(?wxVERTICAL),
    MenuSz = wxBoxSizer:new(?wxHORIZONTAL),
    TextSz = wxBoxSizer:new(?wxHORIZONTAL),

    ReadBn = wxButton:new(Frame, ?sysREAD, [{label, "Read File"}]),
    ConvBn = wxButton:new(Frame, ?sysCONV, [{label, "Convert"}]),
    SaveBn = wxButton:new(Frame, ?sysSAVE, [{label, "Save File"}]),
    JSON   = wxStyledTextCtrl:new(Frame),
    ETerms = wxStyledTextCtrl:new(Frame),
    Mono = wxFont:new(10, ?wxMODERN, ?wxNORMAL, ?wxNORMAL, [{face, "Monospace"}]),
    ok = wxStyledTextCtrl:styleSetFont(JSON,   ?wxSTC_STYLE_DEFAULT, Mono),
    ok = wxStyledTextCtrl:styleSetFont(ETerms, ?wxSTC_STYLE_DEFAULT, Mono),

    _ = wxBoxSizer:add(MenuSz, ReadBn, [{flag, ?wxEXPAND}, {proportion, 1}]),
    _ = wxBoxSizer:add(MenuSz, ConvBn, [{flag, ?wxEXPAND}, {proportion, 1}]),
    _ = wxBoxSizer:add(MenuSz, SaveBn, [{flag, ?wxEXPAND}, {proportion, 1}]),
    _ = wxBoxSizer:add(TextSz, JSON,   [{flag, ?wxEXPAND}, {proportion, 1}]),
    _ = wxBoxSizer:add(TextSz, ETerms, [{flag, ?wxEXPAND}, {proportion, 1}]),
    _ = wxBoxSizer:add(MainSz, MenuSz, [{flag, ?wxEXPAND}, {proportion, 0}]),
    _ = wxBoxSizer:add(MainSz, TextSz, [{flag, ?wxEXPAND}, {proportion, 1}]),
    ok = wxFrame:setSizer(Frame, MainSz),
    ok = wxBoxSizer:layout(MainSz),

    Display = wxDisplay:new(),
    {X, Y, W, H} = wxDisplay:getClientArea(Display),
    ok = wxDisplay:destroy(Display),
    ok = wxFrame:connect(Frame, close_window),
    ok = wxFrame:connect(Frame, command_button_clicked),
    ok = wxFrame:setSize(Frame, {X, Y, (W div 4) * 3, (H div 4) * 3}),
    ok = wxFrame:center(Frame),
    true = wxFrame:show(Frame),
    #s{frame = Frame, json = JSON, eterms = ETerms}.


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().

handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_cast/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_cast-2

handle_cast({json, String}, State) ->
    ok = do_json(String, State),
    {noreply, State};
handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_info/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2

handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


-spec handle_event(Event, State) -> {noreply, NewState}
    when Event    :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The wx_object:handle_event/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2

handle_event(#wx{id = ID, event = #wxCommand{type = command_button_clicked}}, State) ->
    case ID of
        ?sysREAD ->
            NewState = do_read(State),
            {noreply, NewState};
        ?sysCONV ->
            NewState = do_conv(State),
            {noreply, NewState};
        ?sysSAVE ->
            NewState = do_save(State),
            {noreply, NewState}
    end;
handle_event(#wx{event = #wxClose{}}, State = #s{frame = Frame}) ->
    ok = tg_con:stop(),
    ok = wxWindow:destroy(Frame),
    {noreply, State};
handle_event(Event, State) ->
    ok = log(info, "Unexpected event ~tp State: ~tp~n", [Event, State]),
    {noreply, State}.



code_change(_, State, _) ->
    {ok, State}.


terminate(Reason, State) ->
    ok = log(info, "Reason: ~tp, State: ~tp", [Reason, State]),
    wx:destroy().



do_json(String, #s{json = JSON}) ->
    wxStyledTextCtrl:setText(JSON, String).


do_read(State = #s{frame = Frame, json = JSON}) ->
    Dialog = wxFileDialog:new(Frame, [{style, ?wxFD_OPEN}]),
    _ = wxFileDialog:showModal(Dialog),
    Path = wxFileDialog:getPath(Dialog),
    ok = wxFileDialog:destroy(Dialog),
    ok =
        case tg_con:read_file(Path) of
            {ok, Bin} ->
                Text = unicode:characters_to_list(Bin),
                wxStyledTextCtrl:setText(JSON, Text);
            {error, eisdir} ->
                ok;
            {error, enoent} ->
                ok
        end,
    State.


do_conv(State = #s{json = JSON, eterms = ETerms}) ->
    InText = wxStyledTextCtrl:getText(JSON),
    OutText =
        case zj:decode(InText) of
            {ok, Terms} -> format(Terms);
            Error       -> io_lib:format("ERROR: ~tp", [Error])
        end,
    ok = wxStyledTextCtrl:setText(ETerms, OutText),
    State.

format(Terms) when is_list(Terms) ->
    Format = fun(Term) -> io_lib:format("~tp.~n", [Term]) end,
    lists:map(Format, Terms);
format(Term) ->
    format([Term]).


do_save(State = #s{frame = Frame, eterms = ETerms}) ->
    Dialog = wxFileDialog:new(Frame, [{style, ?wxFD_SAVE}]),
    _ = wxFileDialog:showModal(Dialog),
    ok =
        case wxFileDialog:getPath(Dialog) of
            [] ->
                ok;
            Path ->
                Data = wxStyledTextCtrl:getText(ETerms),
                tg_con:save_file(Path, Data)
        end,
    ok = wxFileDialog:destroy(Dialog),
    State.

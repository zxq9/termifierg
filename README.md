This project is the code part of a tutorial about creating GUI projects in Erlang using ZX.

- The tutorial: https://zxq9.com/archives/1650
- The ZX code tool: https://zxq9.com/projects/zomp/
- The Erlang language docs: https://erlang.org/doc/index.html
